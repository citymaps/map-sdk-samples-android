package com.citymaps.example;

import android.graphics.Color;
import android.graphics.PointF;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.citymaps.citymapsengine.CitymapsMapView;
import com.citymaps.citymapsengine.Label;
import com.citymaps.citymapsengine.LonLat;
import com.citymaps.citymapsengine.MapView;
import com.citymaps.citymapsengine.MapViewReadyListener;
import com.citymaps.citymapsengine.Marker;
import com.citymaps.citymapsengine.MarkerGroup;
import com.citymaps.citymapsengine.MarkerOnTouchListener;
import com.citymaps.citymapsengine.SupportCitymapsMapFragment;
import com.citymaps.citymapsengine.events.MarkerEvent;
import com.citymaps.example.util.MapUtils;

public class MarkerSampleFragment extends Fragment
{
    public static final String MARKER_SAMPLE_GROUP = "marker_sample_group";
    SupportCitymapsMapFragment mMapFragment;
    public static MarkerSampleFragment newInstance() {
        return new MarkerSampleFragment();
    }

    public void onResume() {
        super.onResume();
        // Apply state
        mMapFragment = (SupportCitymapsMapFragment)getFragmentManager().findFragmentByTag(Constants.TAG_MAP_FRAGMENT);
        mMapFragment.setMapViewReadyListener(new MapViewReadyListener() {
            @Override
            public void onMapViewReady(MapView mapView) {
                applyState();
            }
        });
    }

    public void onStop() {
        super.onStop();
        // Restore state
        restoreState();
    }

    private void applyState()
    {
        CitymapsMapView mapView = mMapFragment.getMapView();
        mapView.getBusinessLayer().setVisible(false);

        MarkerGroup group = mapView.getMarkerGroup(MARKER_SAMPLE_GROUP);
        MapUtils.setMapCenter(mapView, new LonLat(-73.969373, 40.766262), 17, 0, 0, true, null);

        Marker marker = Marker.createMarkerFromResource(R.drawable.address_poi_pin);
        marker.setPosition(new LonLat(-73.969373, 40.766262));
        marker.setHighlightColor(Color.GRAY);
        marker.setAnchorPoint(new PointF(0.5f, 1.0f));
        marker.setAlpha(0);
        marker.setAlpha(-1);

        Label label = new Label();
        label.setText(getString(R.string.citymaps_world_hq));
        label.setHorizontalAlignment(Label.kLabelHorizontalAlignmentLeft);
        label.setVerticalAlignment(Label.kLabelVerticalAlignmentMiddle);
        label.setAnchorPoint(new PointF(1.0f, 0.5f));

        marker.addLabel(label);

        marker.setOnTouchListener(new MarkerOnTouchListener() {
            @Override
            public boolean onTouchEvent(MarkerEvent markerEvent) {
                switch (markerEvent.getType()) {
                    case MarkerEvent.TAP:
                        Toast.makeText(getActivity(), R.string.marker_tapped, Toast.LENGTH_SHORT).show();
                        break;
                    case MarkerEvent.DOUBLE_TAP:
                        Toast.makeText(getActivity(), R.string.marker_double_tapped, Toast.LENGTH_SHORT).show();
                        break;
                    case MarkerEvent.LONG_PRESS:
                        Toast.makeText(getActivity(), R.string.marker_long_press, Toast.LENGTH_SHORT).show();
                        break;
                }
                return true;
            }
        });
        group.addMarker(marker);
    }

    private void restoreState()
    {
        CitymapsMapView mapView = mMapFragment.getMapView();
        mapView.getBusinessLayer().setVisible(true);
        MarkerGroup group = mapView.getMarkerGroup(MARKER_SAMPLE_GROUP);
        group.removeAllMarkers();
    }
}
