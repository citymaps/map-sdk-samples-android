package com.citymaps.example.util;

import android.os.Parcel;
import android.os.Parcelable;

import com.citymaps.citymapsengine.LonLat;
import com.citymaps.citymapsengine.MapView;
import com.citymaps.citymapsengine.MapViewAnimationListener;

/**
 * Created by eddiekimmel on 4/7/15.
 */
public class MapState implements Parcelable
{
    public static final LonLat DEFAULT_LOCATION = new LonLat(-74, 40.7);
    public static final float DEFAULT_ZOOM = 15;
    public static final float DEFAULT_ROTATION = 0;
    public static final float DEFAULT_TILT = 0;

    private LonLat mLocation = DEFAULT_LOCATION;
    private float mZoom = DEFAULT_ZOOM;
    private float mRotation = DEFAULT_ROTATION;
    private double mTilt = DEFAULT_TILT;

    public MapState() {

    }

    public MapState(MapView mapView) {
        mLocation = mapView.getCenter();
        mZoom = mapView.getZoom();
        mRotation = mapView.getRotation();
        mTilt = mapView.getTilt();
    }

    public MapState(Parcel in) {
        mLocation = in.readParcelable(LonLat.class.getClassLoader());
        if (mLocation == null) {
            mLocation = DEFAULT_LOCATION;
        }
        mZoom = in.readFloat();
        mRotation = in.readFloat();
        mTilt = in.readDouble();
    }

    public void apply(MapView mapView, boolean animated, MapViewAnimationListener listener) {
        MapUtils.setMapCenter(mapView, mLocation, mZoom, mRotation, mTilt, animated, listener);
    }

    public static final Creator<MapState> CREATOR  = new Creator<MapState>() {
        @Override
        public MapState createFromParcel(Parcel source) {
            return new MapState(source);
        }

        @Override
        public MapState[] newArray(int size) {
            return new MapState[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mLocation, flags);
        dest.writeFloat(mZoom);
        dest.writeFloat(mRotation);
        dest.writeDouble(mTilt);
    }
}
