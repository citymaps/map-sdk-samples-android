package com.citymaps.example.util;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import com.citymaps.citymapsengine.*;

import java.util.Collection;
import java.util.List;

public class MapUtils {

	public static  final double ZOOM_TO_BOUNDS_PADDING = 0.2;

	public static float getMapRadius(MapView mapView) {

        if (mapView == null) {
            return 0;
        }

		// See iOS CMMapController.mapRadius
		int size = Math.max(mapView.getWidth(), mapView.getHeight() / 2);
		return (float) mapView.getResolution() * size * 0.001f;
	}

	public static void setMapCenter(MapView mapView, LonLat center)
	{
		setMapCenter(mapView, center, -1, -1, -1, true, null);
	}

	public static void setMapCenter(MapView mapView, LonLat center, float zoom)
	{
		setMapCenter(mapView, center, zoom, -1, -1, true, null);
	}

    public static void setMapCenter(MapView mapView, LonLat center, float zoom, double rotation, double tilt)
    {
        setMapCenter(mapView, center, zoom, rotation, tilt, true, null);
    }

	public static void setMapCenter(MapView mapView, LonLat center, float zoom, double rotation, double tilt, boolean animated, MapViewAnimationListener listener)
	{
        if (mapView == null) {
            return;
        }

		if (center == null)
		{
			center = mapView.getCenter();
		}

		float currentZoom = mapView.getZoom();
		if (zoom < 0.1)
		{
			zoom = currentZoom;
		}

		if (rotation < 0) {
            rotation = mapView.getOrientation();
        }

		if (tilt < 0)
		{
			tilt = mapView.getTilt();
		}

		long duration = 0;

		if (animated)
		{
			LonLat currentCenter = mapView.getCenter();
			double meters = currentCenter.getDistanceMeters(center);
			double pixels = meters / mapView.getResolution();
			double pixelsAfterZoom = meters / MapUtil.getResolutionForZoom(zoom);

			if (zoom < currentZoom || pixels < 300)
			{
				pixels = (pixels + pixelsAfterZoom) * 0.5;
			}

			duration = (long)pixels;
			duration = Math.max(duration, 300);
			duration = Math.min(duration, 3000);
		}

        mapView.setTrackingMode(MapView.MAP_TRACKING_MODE_NONE);
		mapView.setMapPosition(new MapPosition(center, zoom, rotation, tilt), duration, listener);
	}

	public static void zoomToLocations(MapView mapView, Collection<LonLat> locations, boolean animated, MapViewAnimationListener listener)
	{
        if (mapView == null) {
            return;
        }

		if (locations.size() == 0)
		{
			if (listener != null)
			{
				listener.onAnimationEnd(false);
			}
		}
		else if (locations.size() == 1)
		{
			MapUtils.setMapCenter(mapView, locations.iterator().next());
		}
		else
		{
			double minLat = 90;
			double maxLat = -90;
			double minLon = 180;
			double maxLon = -180;

			for (LonLat pos : locations)
			{
				minLat = Math.min(pos.latitude, minLat);
				maxLat = Math.max(pos.latitude, maxLat);
				minLon = Math.min(pos.longitude, minLon);
				maxLon = Math.max(pos.longitude, maxLon);
			}

			double deltaLat = (maxLat - minLat) * ZOOM_TO_BOUNDS_PADDING;
			double deltaLon = (maxLon - maxLat) * ZOOM_TO_BOUNDS_PADDING;

			LonLatBounds bounds = new LonLatBounds(minLon - deltaLon, minLat - deltaLat, maxLon - deltaLon, maxLat - deltaLat);

			float zoom = mapView.getZoomContainingBounds(bounds);

			setMapCenter(mapView, bounds.getCenter(), zoom, 0, 0, animated, listener);
		}
	}

    public static boolean checkLocationServices(Context context) {
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static LonLat getUserLocation(MapView mapView)
    {
        if (mapView == null){
            return null;
        }
        UserLocation location = mapView.getUserLocation();
        if (location != null && location.position != null && location.position.getDistanceMeters(new LonLat(0,0)) > 1)
        {
            return location.position;
        }
        return null;
    }

	private MapUtils() {
	}

    public static Location findBestLocation(Context context) {
        Location bestLocation = null;

        LocationManager locationManager = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = locationManager.getAllProviders();
        for (String provider : providers) {
            Location location = locationManager.getLastKnownLocation(provider);
            if (location != null) {
                if (bestLocation == null) {
                    bestLocation = location;
                } else {
                    if (location.getElapsedRealtimeNanos() > bestLocation.getElapsedRealtimeNanos()) {
                        // Newer
                        bestLocation = location;
                    }
                }
            }
        }

        return bestLocation;
    }
}
