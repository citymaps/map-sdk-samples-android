package com.citymaps.example;

import android.graphics.Color;
import android.support.v4.app.Fragment;

import com.citymaps.citymapsengine.CircleFeature;
import com.citymaps.citymapsengine.CitymapsMapView;
import com.citymaps.citymapsengine.FeatureGroup;
import com.citymaps.citymapsengine.LineFeature;
import com.citymaps.citymapsengine.LonLat;
import com.citymaps.citymapsengine.MapView;
import com.citymaps.citymapsengine.MapViewReadyListener;
import com.citymaps.citymapsengine.SupportCitymapsMapFragment;
import com.citymaps.citymapsengine.options.CircleFeatureOptions;
import com.citymaps.citymapsengine.options.LineFeatureOptions;
import com.citymaps.example.util.MapUtils;

/**
 * Created by eddiekimmel on 4/7/15.
 */
public class CanvasSampleFragment extends Fragment
{
    SupportCitymapsMapFragment mMapFragment;

    public static final String CANVAS_EXAMPLE_FEATURE_GROUP = "canvas_example_features";

    private LineFeature mLineFeature;
    private CircleFeature mCircleFeature;

    public static CanvasSampleFragment newInstance() {
        return new CanvasSampleFragment();
    }

    public void onResume() {
        super.onResume();
        // Apply state
        mMapFragment = (SupportCitymapsMapFragment)getFragmentManager().findFragmentByTag(Constants.TAG_MAP_FRAGMENT);
        mMapFragment.setMapViewReadyListener(new MapViewReadyListener() {
            @Override
            public void onMapViewReady(MapView mapView) {
                applyState();
            }
        });
    }

    public void onStop() {
        super.onStop();
        restoreState();
    }

    private void applyState()
    {
        CitymapsMapView mapView = mMapFragment.getMapView();
        FeatureGroup group = mapView.getFeatureGroup(CANVAS_EXAMPLE_FEATURE_GROUP);
        MapUtils.setMapCenter(mapView, new LonLat(-74.05, 40.75), 15, 0, 0, true, null);

        if (mLineFeature == null) {
            LineFeatureOptions options = new LineFeatureOptions();
            options.decimate(true)
                    .add(new LonLat(-74.05, 40.75), new LonLat(-74.1, 40.71), new LonLat(-74.1, 40.8), new LonLat(-74.2, 40.8), new LonLat(-74.0, 40.6), new LonLat(-73.9, 40.71),
                        new  LonLat(-74.05, 40.75))
                    .fillWidth(5)
                    .strokeWidth(2)
                    .fillColor(Color.RED)
                    .strokeColor(Color.BLACK);
            mLineFeature = new LineFeature(options);
            group.addFeature(mLineFeature);
        }

        if (mCircleFeature == null) {
            CircleFeatureOptions options = new CircleFeatureOptions();
            options.center(new LonLat(-74.05, 40.75)).radius(500).strokeWidth(10).fillColor(Color.argb(127, 255, 0, 0))
                    .strokeColor(Color.BLUE);
            mCircleFeature = new CircleFeature(options);
            group.addFeature(mCircleFeature);
        }
    }

    private void restoreState()
    {
        CitymapsMapView mapView = mMapFragment.getMapView();
        FeatureGroup group = mapView.getFeatureGroup(CANVAS_EXAMPLE_FEATURE_GROUP);
        group.removeAllFeature();

        mLineFeature = null;
        mCircleFeature = null;
    }
}
