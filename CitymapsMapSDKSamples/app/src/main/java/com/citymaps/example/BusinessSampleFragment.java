package com.citymaps.example;

import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.widget.Toast;

import com.citymaps.citymapsengine.BusinessLayerListener;
import com.citymaps.citymapsengine.CitymapsMapView;
import com.citymaps.citymapsengine.LonLat;
import com.citymaps.citymapsengine.MapView;
import com.citymaps.citymapsengine.MapViewReadyListener;
import com.citymaps.citymapsengine.MarkerGroup;
import com.citymaps.citymapsengine.SupportCitymapsMapFragment;
import com.citymaps.citymapsengine.events.BusinessEvent;
import com.citymaps.citymapsengine.events.BusinessTouchEvent;
import com.citymaps.example.util.MapUtils;

public class BusinessSampleFragment extends Fragment
{
    SupportCitymapsMapFragment mMapFragment;
    public static BusinessSampleFragment newInstance() {
        return new BusinessSampleFragment();
    }

    public void onResume() {
        super.onResume();
        // Apply state
        mMapFragment = (SupportCitymapsMapFragment)getFragmentManager().findFragmentByTag(Constants.TAG_MAP_FRAGMENT);
        mMapFragment.setMapViewReadyListener(new MapViewReadyListener() {
            @Override
            public void onMapViewReady(MapView mapView) {
                applyState();
            }
        });
    }

    public void onStop() {
        super.onStop();
        // Restore state
        restoreState();
    }

    private void applyState()
    {
        CitymapsMapView mapView = mMapFragment.getMapView();
        MapUtils.setMapCenter(mapView, new LonLat(-73.969373, 40.766262), 17, 0, 0, true, null);

        mapView.getBusinessLayer().setBusinessListener(new BusinessLayerListener() {
            @Override
            public void onBusinessEvent(BusinessEvent businessEvent) {
                // This method will report when a business is shown or hidden.
                // You can use this if you need to keep record of the on screen businesses, or need to apply logic for specific businesses on screen.
            }

            @Override
            public void onTouchEvent(BusinessTouchEvent businessTouchEvent) {

                String text = null;
                switch (businessTouchEvent.getType()) {
                    case BusinessTouchEvent.TAP:
                        text = getString(R.string.business_tapped, businessTouchEvent.getBusiness().getName());
                        break;
                    case BusinessTouchEvent.DOUBLE_TAP:
                        text = getString(R.string.business_double_tapped, businessTouchEvent.getBusiness().getName());
                        break;
                    case BusinessTouchEvent.LONG_PRESS:
                        text = getString(R.string.business_long_press, businessTouchEvent.getBusiness().getName());
                        break;
                    default:
                        break;
                }

                if (!TextUtils.isEmpty(text)) {
                    Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void restoreState()
    {
        CitymapsMapView mapView = mMapFragment.getMapView();
        mapView.getBusinessLayer().setBusinessListener(null);
    }
}
