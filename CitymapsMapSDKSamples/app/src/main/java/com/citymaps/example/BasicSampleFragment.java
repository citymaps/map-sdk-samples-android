package com.citymaps.example;

import android.support.v4.app.Fragment;

import com.citymaps.citymapsengine.MapView;
import com.citymaps.citymapsengine.MapViewReadyListener;
import com.citymaps.citymapsengine.SupportCitymapsMapFragment;

public class BasicSampleFragment extends Fragment
{
    SupportCitymapsMapFragment mMapFragment;
    public static BasicSampleFragment newInstance() {
        return new BasicSampleFragment();
    }

    public void onResume() {
        super.onResume();
        // Apply state
        mMapFragment = (SupportCitymapsMapFragment)getFragmentManager().findFragmentByTag(Constants.TAG_MAP_FRAGMENT);
        mMapFragment.setMapViewReadyListener(new MapViewReadyListener() {
            @Override
            public void onMapViewReady(MapView mapView) {
                applyState();
            }
        });
    }

    public void onStop() {
        super.onStop();
        // Restore state
        restoreState();
    }

    private void applyState()
    {

    }

    private void restoreState()
    {

    }
}
