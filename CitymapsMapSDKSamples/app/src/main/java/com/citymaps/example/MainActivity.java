package com.citymaps.example;

import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;

import com.citymaps.citymapsengine.SupportCitymapsMapFragment;
import com.citymaps.citymapsengine.WindowAndroid;
import com.citymaps.citymapsengine.options.CitymapsMapViewOptions;
import com.citymaps.example.util.MapState;

public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    private static final int SAMPLE_BASIC = 0;
    private static final int SAMPLE_CANVAS = 1;
    private static final int SAMPLE_MARKER = 2;
    private static final int SAMPLE_BUSINESSES = 3;
    private static final int SNAPSHOT = 4;

    private static final String STATE_KEY_MAP_STATE = "map_state";

    private NavigationDrawerFragment mNavigationDrawerFragment;
    private SupportCitymapsMapFragment mMapFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        mTitle = getTitle();

        if (savedInstanceState != null) {
            mMapFragment = (SupportCitymapsMapFragment)getSupportFragmentManager().findFragmentByTag(Constants.TAG_MAP_FRAGMENT);
        }

        if (mMapFragment == null) {
            CitymapsMapViewOptions options = new CitymapsMapViewOptions();
            mMapFragment = SupportCitymapsMapFragment.newInstance(options);
            mMapFragment.setRetainInstance(true);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.map_container, mMapFragment, Constants.TAG_MAP_FRAGMENT)
                    .commit();
        }

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();

        Fragment newFragment = null;
        switch (position) {
            case SAMPLE_BASIC: {
                newFragment = BasicSampleFragment.newInstance();
                break;
            }

            case SAMPLE_CANVAS: {
                newFragment = CanvasSampleFragment.newInstance();
            }
                break;
            case SAMPLE_MARKER: {
                newFragment = MarkerSampleFragment.newInstance();
            }
                break;
            case SAMPLE_BUSINESSES:
                newFragment = BusinessSampleFragment.newInstance();
                break;

            case SNAPSHOT: {
                mMapFragment.getMapView().requestSnapshot(new WindowAndroid.SnapshotReadyListener() {
                    @Override
                    public void onSnapshotReady(Bitmap bitmap) {
                        SnapshotFragment dialog = new SnapshotFragment();
                        dialog.setImage(bitmap);
                        dialog.show(getSupportFragmentManager(), "snapshot");
                    }
                });
                break;
            }
        }

        if (newFragment != null) {
            fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, newFragment)
                .commit();
        }
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.sample_basic);
                break;
            case 2:
                mTitle = getString(R.string.sample_basic);
                break;
            case 3:
                mTitle = getString(R.string.sample_basic);
                break;
            case 4:
                mTitle = getString(R.string.sample_basic);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }
}
